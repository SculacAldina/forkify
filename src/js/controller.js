import * as model from './model.js';
import recipeView from './views/recipeView.js';
import searchView from './views/searchView.js';
import resultsView from './views/resultsView.js';
import paginationView from './views/paginationView.js';
import bookmarksView from './views/bookmarksView.js';
import addRecipeView from './views/addRecipeView.js';

// Polyfills - omogucavaju da stari browseri mogu koristiti ES6
import 'core-js/stable'; // Polyfill for everything else
import 'regenerator-runtime/runtime'; // Polyfill for async await

///////////////////////////////////////
// Parcel
// automatically updating modules in the browser at runtime without needing a whole page refresh.
if(module.hot) {
  module.hot.accept();
}


// Controller - Fetch a recipe
const controlRecipes = async function () {
  try {
    // get ID from hash
    const id = window.location.hash.slice(1);
    if (!id) return;

    recipeView.renderSpinner();
    
    // 0. Update results view to mark selected search result
    resultsView.update(model.getSearchResultsPage());

    // 1. Updating bookmarks view
    bookmarksView.update(model.state.bookmarks);

    // 2. Loading recipe
    await model.loadRecipe(id);

    // 3. Rendering the recipe
    recipeView.render(model.state.recipe);


  } catch (err) {
    recipeView.renderError();
    console.log(err);
  }

};


// Controller - Search
const controlSearchResults = async function(){
  try{
    resultsView.renderSpinner();
    // 1. Get search query
    const query = searchView.getQuery();
    if(!query) return;

    // 2. Get search results
    await model.loadSearchResults(query);
    
    // 3. Render results
    // console.log(model.state.search.results);
    // resultsView.render(model.state.search.results);
    resultsView.render(model.getSearchResultsPage());

    // 4. Render initial pagination buttons
    paginationView.render(model.state.search);
  } catch (e){
    console.log(e)
  }
}


// Controller for Btn clicked
const controlPagination = function(goToPage){
  // console.log(goToPage);
  // 1. Render NEW results
  resultsView.render(model.getSearchResultsPage(goToPage));

  // 2. Render NEW Pagination buttons
  paginationView.render(model.state.search);
}


// Controller - Servings
const controlServings = function(newServings){
  // Update the recipe servings state
  model.updateServings(newServings);

  // Update the view
  // recipeView.render(model.state.recipe);
  recipeView.update(model.state.recipe);
}


// Controller - Bookmark
const controlAddBookmark = function(){
  // 1. Add/remove bookmark
  if(!model.state.recipe.bookmarked){
    model.addBookmark(model.state.recipe);
  }else {
    model.deleteBookmark(model.state.recipe.id);
  }
  // console.log(model.state.recipe);
  // 2. Update the view
  recipeView.update(model.state.recipe);

  // 3. Render bookmarks
  bookmarksView.render(model.state.bookmarks);
}


const controlBookmarks = function(){
  bookmarksView.render(model.state.bookmarks);
}


const controlAddRecipe = function(newRecepie){
  console.log(newRecepie);
}

const init = function(){
  // Event Handlers in MVC: Publisher-Subscriber Pattern
  bookmarksView.addHandlerRender(controlBookmarks);
  recipeView.addHandlerRender(controlRecipes);
  recipeView.addHandlerUpdateServings(controlServings);
  recipeView.addHandlerAddBookmark(controlAddBookmark);
  searchView.addHandlerSearch(controlSearchResults);
  paginationView.addHandlerClick(controlPagination);
  addRecipeView.addHandlerUpload(controlAddRecipe);

  // Upload the new recipe data
}
init();
// window.addEventListener('hashchange', controlRecipes);
// window.addEventListener('load', controlRecipes);
