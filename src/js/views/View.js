import icons from 'url:../../img/icons.svg';

export default class View {
  _data;

  render(data, render = true) {
    if(!data || (Array.isArray(data) && data.length === 0)) return this.renderError(); // check if data exists

    this._data = data;
    const markup = this._generateMarkup();

    if(!render) return markup;

    this._clear(); // clear container
    this._parentElement.insertAdjacentHTML('afterbegin', markup);
  }

  // Developing a DOM Updating Algorithm
  update(data){
    this._data = data;
    // usporediti old i new markupa i napraviti update samo na promijenjenim dijelovima
    const newMarkup = this._generateMarkup(); // string
    const newDOM = document.createRange().createContextualFragment(newMarkup); // DOM object
    const newElements = Array.from(newDOM.querySelectorAll('*')); // select all elements // convert to real array Array.from()
    const curElements = Array.from(this._parentElement.querySelectorAll('*')); // convert to real array Array.from()

    // console.log(curElements);
    // console.log(newElements);

    newElements.forEach((newEl, i) => {
      const curEl = curElements[i];
      // console.log(curEl, newEl.isEqualNode(curEl));

      // Updates changed TEXT
      if (
        !newEl.isEqualNode(curEl) &&
        newEl.firstChild?.nodeValue.trim() !== ''
      ) {
        // console.log('💥', newEl.firstChild.nodeValue.trim());
        curEl.textContent = newEl.textContent;
      }

      // Updates changed ATTRIBUES
      if (!newEl.isEqualNode(curEl)){
        Array.from(newEl.attributes).forEach(attr =>
          curEl.setAttribute(attr.name, attr.value)
        );
      }
    })
  }

  _clear() {
    this._parentElement.innerHTML = '';
  }

  // Spinner
  renderSpinner() {
    const markup = `<div class="spinner">
            <svg>
              <use href="${icons}#icon-loader"></use>
            </svg>
          </div>`;
    this._clear(); // clear container
    this._parentElement.insertAdjacentHTML('afterbegin', markup);
  }

  renderError(message = this._errorMessage){
    const markup = `<div class="error">
      <div>
        <svg>
          <use href="${icons}.svg#icon-alert-triangle"></use>
        </svg>
      </div>
      <p>${message}</p>
    </div>`;
    this._clear(); // clear container
    this._parentElement.insertAdjacentHTML('afterbegin', markup);
  }

  renderMessage(message = this._message){
    const markup = `<div class="message">
      <div>
        <svg>
          <use href="${icons}.svg#icon-smile"></use>
        </svg>
      </div>
      <p>${message}</p>
    </div>`;
    this._clear(); // clear container
    this._parentElement.insertAdjacentHTML('afterbegin', markup);
  }
}