## Forkify JS App

Install dependencies

```bash
  npm install
```

Run app locally

```bash
  npm run start
```
